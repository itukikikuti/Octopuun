﻿using UnityEngine;
using System.Collections;

public class Floor : MonoBehaviour
{
	private Texture2D texture;
	public const int textureWidth = 1000;
	public const int textureHeight = 1000;
	public Rect fillRect;

	void Awake()
	{
		texture = new Texture2D(textureWidth, textureHeight);

		GetComponent<MeshRenderer>().materials[0].mainTexture = texture;
	}

	void Update()
	{
		if (GameObject.Find("Player").transform.position.y <= 0.6f &&
			GameObject.Find("Player").transform.GetChild(0).localScale.y >= 0.9f)
		{
			float playerx = GameObject.Find("Player").transform.position.x * 10f - 500f;
			float playerz = GameObject.Find("Player").transform.position.z * 10f - 500f;
			float playerRight = -GameObject.Find("Player").transform.eulerAngles.y * Mathf.Deg2Rad;
			float playerBack = -(GameObject.Find("Player").transform.eulerAngles.y + 90f) * Mathf.Deg2Rad;
			Color color = GameObject.Find("Player").GetComponent<Player>().color;

			for (int z = (int)fillRect.yMin; z <= (int)fillRect.yMax; ++z)
			{
				for (int x = (int)fillRect.xMin; x <= (int)fillRect.xMax; ++x)
				{
					texture.SetPixel((int)(playerx + Mathf.Cos(playerRight) * (x * 0.5f) + Mathf.Cos(playerBack) * (z * 0.5f) + Random.Range(-3f, 3f)), (int)(playerz + Mathf.Sin(playerRight) * (x * 0.5f) + Mathf.Sin(playerBack) * (z * 0.5f) + Random.Range(-3f, 3f)), color);
				}
			}

			texture.Apply();
		}
	}

	public bool GetFill(Vector3 position)
	{
		Color color = GameObject.Find("Player").GetComponent<Player>().color;

		if (texture.GetPixel((int)(position.x * 10f - 500f), (int)(position.z * 10f - 500f)) == color)
		{
			return true;
		}

		return false;
	}
}
