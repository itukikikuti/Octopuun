﻿using UnityEngine;

public class Player : MonoBehaviour
{
	public float humanSpeed;
	public float octopusSpeed;
	public Color color;
	private float speed = 0f;

    private void Awake()
	{
		Cursor.lockState = CursorLockMode.Locked;
		speed = humanSpeed;
    }

	private void Update()
	{
		GetComponentInChildren<MeshRenderer>().material.color = color;

		transform.Rotate(0f, Input.GetAxis("Mouse X") * 5f, 0f);

		if (Input.GetKeyDown(KeyCode.LeftShift))
		{
			GetComponent<Rigidbody>().AddForce(new Vector3(0f, -5f, 0f), ForceMode.Impulse);
			transform.GetChild(0).localScale = new Vector3(1f, 0.2f, 1f);
		}

		if (Input.GetKey(KeyCode.LeftShift))
		{
			if (transform.position.y <= 0.3f)
			{
				if (GameObject.Find("Floor").GetComponent<Floor>().GetFill(transform.position))
				{
					GetComponentInChildren<MeshRenderer>().enabled = false;
					speed = octopusSpeed;
				}
				else
				{
					GetComponentInChildren<MeshRenderer>().enabled = true;
					speed = humanSpeed / 2f;
				}
			}
			else
			{
				GetComponentInChildren<MeshRenderer>().enabled = true;

				speed = octopusSpeed;
			}
		}

		if (Input.GetKeyUp(KeyCode.LeftShift))
		{
			GetComponentInChildren<MeshRenderer>().enabled = true;
			transform.GetChild(0).localScale = new Vector3(1f, 1f, 1f);
			speed = humanSpeed;
		}

		if (Input.GetKey(KeyCode.W))
		{
			GetComponent<Rigidbody>().AddForce(transform.forward * speed, ForceMode.Impulse);
		}

		if (Input.GetKey(KeyCode.S))
		{
			GetComponent<Rigidbody>().AddForce(-transform.forward * speed, ForceMode.Impulse);
		}

		if (Input.GetKey(KeyCode.A))
		{
			GetComponent<Rigidbody>().AddForce(-transform.right * speed, ForceMode.Impulse);
		}

		if (Input.GetKey(KeyCode.D))
		{
			GetComponent<Rigidbody>().AddForce(transform.right * speed, ForceMode.Impulse);
		}

		if (Input.GetKeyDown(KeyCode.Space))
		{
			GetComponent<Rigidbody>().AddForce(new Vector3(0f, 7f, 0f), ForceMode.Impulse);
		}
	}
}
